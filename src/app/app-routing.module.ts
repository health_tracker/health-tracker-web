import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { DrsComponent } from './drs/drs.component';
import { BpsComponent } from './bps/bps.component';

const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'bps', component: BpsComponent },
  { path: 'drs', component: DrsComponent},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: '', redirectTo: 'welcome', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { enableTracing: true }//used for debugging only
  )],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }
